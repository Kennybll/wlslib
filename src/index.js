import PickerComponent from './emoji/Picker';
import EmojifyFunction from './emoji/Emojify';

export const Picker = PickerComponent;
export const Emojify = EmojifyFunction;