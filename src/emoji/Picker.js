import React from 'react';
import data from 'emoji-mart/data/twitter.json';
import { NimblePicker } from 'emoji-mart';
import customEmojis from './CustomImages.json';

export default (props) => <NimblePicker set='twitter' custom={customEmojis} data={data} {...props} />;