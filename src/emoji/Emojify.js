import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { Emoji } from 'emoji-mart';
import customEmojis from './CustomImagesObject.json';
const emojiNameRegex = /:([a-zA-Z0-9_\-\+]+):/g;

export default (string) => {
  return string.split(emojiNameRegex)
    .map((emojiInstance, i) => {
      if (i % 2 === 0) return emojiInstance;
        if(emojiInstance.startsWith('wls')) {
          return ReactDOMServer.renderToString(<span dangerouslySetInnerHTML={{
            __html: Emoji({
              html: true,
              emoji: customEmojis[emojiInstance],
              size: 32
            })
          }}></span>);
        }
        return ReactDOMServer.renderToString(<span dangerouslySetInnerHTML={{
          __html: Emoji({
            html: true,
            emoji: emojiInstance,
            size: 32,
            set: "twitter"
          })
        }}></span>);
    })
    .join('');
}