const fs = require('fs');
const path = require('path');

const images = fs.readdirSync(path.resolve(__dirname, 'images'));

const names = images.map(name => ({
  name: `${name.split('.')[0].split('_').map(title => title.substr(0, 1).toUpperCase() + title.substr(1, title.length - 1)).join(' ')}`,
  short_names: [`wls_${name.split('.')[0]}`],
  text: '',
  emoticons: [],
  keywords: name.split('.')[0].split('_'),
  imageUrl: `https://gitlab.com/Kennybll/wlslib/raw/master/src/emoji/images/${name}`
}));

const namesObject = {};

images.forEach(name => {
  namesObject[`wls_${name.split('.')[0]}`] = {
    name: `${name.split('.')[0].split('_').map(title => title.substr(0, 1).toUpperCase() + title.substr(1, title.length - 1)).join(' ')}`,
    short_names: [`wls_${name.split('.')[0]}`],
    text: '',
    emoticons: [],
    keywords: name.split('.')[0].split('_'),
    imageUrl: `https://gitlab.com/Kennybll/wlslib/raw/master/src/emoji/images/${name}`
  };
})

fs.writeFileSync(
  path.resolve(__dirname, 'CustomImages.json'),
  JSON.stringify(names)
);

fs.writeFileSync(
  path.resolve(__dirname, 'CustomImagesObject.json'),
  JSON.stringify(namesObject)
);